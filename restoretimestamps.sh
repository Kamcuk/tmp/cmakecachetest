#!/bin/sh

gen() {
	find "$@" -path ./.git -prune -o -type f -printf '@%T@\n' -exec md5sum {} \+ |
	awk 'NF==1{a[NR]=$0} NF==2{print a[++idx] "\t" gensub("  ", "\t", 1)}' |
	sort -k3
}

statef=$1
mode=$2
shift 2
case "$mode" in
restore)
	if [ "$#" -eq 0 ]; then
		set -- .
	fi
	if [ -e "$statef" ]; then
		gen |
			join -t"$(printf "\t")" -13 -23 -o 1.2,2.2,2.1,2.3 - "$statef" |
			awk '$1 == $2{print $3, $4}' |
			xargs -r -l -t touch -hcd
	fi
	;;
store)
	mkdir -p "$(dirname "$statef")"
	gen "$@" > "$statef"

	echo ----------------------------------------
	cat "$statef"
	echo ----------------------------------------
	;;
*) echo invalid mode >&2; exit 1; ;;
esac
